#include <Arduino.h>
#include <SPI.h>
#include <Ethernet.h>
#include <ThingSpeak.h>

#define POWER_PIN 13
#define PIN_COUNT 2
#define SMOOTHING 5
#define AVG_VALUES 5

int PINS[] = {A0, A1};
int AVG_MOISTURE[PIN_COUNT][AVG_VALUES];
int currentIdx = 0;

byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
};
EthernetClient client;

int readData(int pin) {
  int data_sum = 0;
  for(int r=0; r<=SMOOTHING; r++) {
      int data = analogRead(pin);
      if (r == 0) {
        delay(10); // discard data (pin switched)
      } else {
        if (r < SMOOTHING) {
          delay(1);
        }
        data_sum += data;
      }
  }
  return data_sum / SMOOTHING;
}

void readAllData() {
  digitalWrite(POWER_PIN, HIGH); // Power on!
  for (int p=0; p<PIN_COUNT; p++) {
    AVG_MOISTURE[p][currentIdx] = readData(PINS[p]);
    currentIdx++;
    if (currentIdx == AVG_VALUES) {
      currentIdx = 0;
    }
  }
  digitalWrite(POWER_PIN, LOW); // Power off.
}

void writeData() {
  for (int p=0; p<PIN_COUNT; p++) {
    int sum_readings = 0;
    int valid_readings = 0;
    for (int v=0; v<AVG_VALUES; v++) {
      int reading = AVG_MOISTURE[p][v];
      if (reading != -1) {
        sum_readings += reading;
        valid_readings++;
      }
    }
    Serial.print(sum_readings / valid_readings);
    if (p+1 < PIN_COUNT) {
      Serial.print(",");
    } else {
      Serial.println();
    }
  }
}

void printIPAddress() {
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }
  Serial.println();
}

void setup() {
  Serial.begin(115200);
  pinMode(POWER_PIN, OUTPUT);
  for (int p=0; p < PIN_COUNT; p++) {
    pinMode(PINS[p], INPUT);
    for(int v=0; v<AVG_VALUES; v++) {
      AVG_MOISTURE[p][v] = -1; // init as -1
    }
  }
  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // no point in carrying on, so do nothing forevermore:
    for (;;)
      ;
  }
  printIPAddress();
  ThingSpeak.begin(client);
}

void loop() {
  Ethernet.maintain();
  if (Serial.read() == -1)
    return;
  readAllData();
  writeData();
}
